﻿DROP SCHEMA public CASCADE;
CREATE SCHEMA public;

-- drop sequence info_serial, target_serial;
CREATE SEQUENCE users_serial START 1;
CREATE SEQUENCE notes_serial START 1;
CREATE SEQUENCE tags_serial START 1;

create table public.users(
   id serial primary key,
   name VARCHAR (50) NOT NULL
);

create table public.notes(
    id serial primary key,
    hash VARCHAR (32) NOT NULL,
    creation_date TIMESTAMP NOT NULL,
    modification_date TIMESTAMP NOT NULL,
    description VARCHAR (200),
    creator_name VARCHAR (32) NOT NULL,
    creator_id int NOT NULL
);

create table public.user_notes(
   user_id serial references public.users(id),
   note_id serial references public.notes(id)
);


 -- PREVENTS DUPLICATE NOTES BEING INSERTED 
 create or replace function prevent_duplicated_inserts() returns setof TRIGGER AS $prevent_duplicated_inserts$
 DECLARE
	leval integer;
    begin
    	select count(*) from notes where hash LIKE NEW.hash into leval;
         if leval > 0 THEN
        -- RAISE EXCEPTION '% Already in database', leval;
        	 return null;       
        ELSE
        	return NEW;
        END IF;
    END;
$prevent_duplicated_inserts$ language plpgsql;
    
 CREATE TRIGGER prevent_Duplicated_Inserts
    Before INSERT ON notes 
    FOR EACH ROW
    EXECUTE PROCEDURE prevent_duplicated_inserts();

 
 -- DELETES REFERENCE TO THE DELETED NOTE
 create or replace function delete_user_notes() returns trigger AS $delete_user_notes$
BEGIN
    delete from user_notes where note_id = OLD.id;
	return OLD;
END;
$delete_user_notes$ language plpgsql;
    
 CREATE TRIGGER delete_notes
    BEFORE DELETE ON notes 
    FOR EACH ROW
    EXECUTE PROCEDURE delete_user_notes();


insert into users values(nextval('users_serial'),'Pedro');
insert into users values(nextval('users_serial'),'Miguel');

-- insert into notes values(nextval('notes_serial'), 'RANDOMstringTHATwillBEhash3', (SELECT LOCALTIMESTAMP(0)), (SELECT LOCALTIMESTAMP(0)), 'Note description', 'creator name', 100);
-- insert into user_notes values((SELECT u.id from users u where u.name ILIKE 'Miguel'), (SELECT n.id from notes n where n.hash ILIKE 'RANDOMstringTHATwillBEhash3')) returning 1;
