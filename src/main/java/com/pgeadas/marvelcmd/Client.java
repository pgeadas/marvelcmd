package com.pgeadas.marvelcmd;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author geadas
 *
 * Default URI will be:
 * http://localhost:8080/MarvelSuperHeroesAPI/v1/{username}/{resource_name}/{operation}
 *
 * * The name of most the functions here are pretty much self-explanatory. Very
 * easy command line tool used to send requests to our backend application
 * server!
 *
 * * Command Line Tool * *
 */
public class Client {

    private static final boolean debugRequests = true;
    //pretty printing enabled for the responses received by this client?
    private static final boolean PRETTY = true;
    private static final Gson gson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
    //needed vars
    private static final Map<String, String> parameters = new LinkedHashMap();
    private static HttpCaller httpCaller;
    private static BufferedReader bi;
    private static StringTokenizer st = null;
    //Default host, port, App name
    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 8080;
    private static final String DEFAULT_SERVICE_NAME = "MarvelSuperHeroesAPI";
    private static String DEFAULT_SERVICE_VERSION = "v1";
    //if no username is suplied, we use the default. New Users need to be added manually to the database.
    private static String DEFAULT_USER = "Pedro";
    //
    private static final String SERVER_LOCAL_ADDRESS_FORMAT = "http://%s:%s/%s/%s";
    //subdomain (appname), domain, api version
    private static final String SERVER_REMOTE_ADDRESS_FORMAT = "http://%s.%s/%s";
    private static String DEFAULT_SERVICE_URI = String.format(SERVER_LOCAL_ADDRESS_FORMAT, DEFAULT_HOST, DEFAULT_PORT, DEFAULT_SERVICE_NAME, DEFAULT_SERVICE_VERSION);
    //resource and operations name for notes
    private static String ADD = "%s/%s/notes/add";
    private static String UPDATE = "%s/%s/notes/update";
    private static String DELETE = "%s/%s/notes/delete";
    private static String EDIT = "%s/%s/notes/edit";
    private static String LISTN = "%s/%s/notes/list";
    //creators
    private static String LISTC = "%s/%s/creators/list";
    private static String COMP = "%s/%s/creators/compare";
    //command parameters names
    private static final String[] addKeys = {"creatorId", "description"};
    private static final String[] delKeys = {"hash"};
    private static final String[] editKeys = {"hash", "description"};
    //some details about the connection
    private static final String contentType = "application/json";
    private static final int connectionTimeout = 5000;
    //messages to be displayed when something is wrong and the keyword when user wants to exit
    private static final String exitTag = "exit";
    private static final String invalidCommand = "Command '%s' not recognised!";
    private static final String invalidArgNumber = "Invalid number of arguments: %d expected, %s found.";
    //command tags
    private static final String helpCommandTag = "help";
    private static final String helpAllCommandTag = "help-all";
    private static final String listCreatorsCommandTag = "listc";
    private static final String listNotesCommandTag = "listn";
    private static final String addCommandTag = "add";
    private static final String editCommandTag = "edit";
    private static final String deleteCommandTag = "del";
    private static final String compareCommandTag = "comp";

    //Maximum number of seconds that we should try to wait for a response from the Marvel API call
    //private static final int DEFAULT_TIMEOUT = 60;
    private static boolean hasStarted = false;

    static final Printer p = new Printer();

    /**
     * @param args
     */
    public static void main(String[] args) {
        //Checks if the arguments provided match the expected and perform the respective action
        checkArgs(args);
        //formats the endpoint uris according to the information provided in the arguments
        formatURIVariables();
        //http client that will connect to the endpoints defined 
        httpCaller = new HttpCaller(p);
        //add the existing commands to the map
        addCommands();
        //starts waiting for user input
        start();
    }

    private static void checkArgs(String[] args) {
        if (args.length == 0) {
            exit();
        }

        switch (args[0]) {
            case "local":
                if (args.length == 6) {
                    printLocalInfo(args[1], args[2], args[3], args[4], args[5]);
                    break;
                }
                exit();
            case "remote":
                if (args.length == 5) {
                    printRemoteInfo(args[1], args[2], args[3], args[4]);
                    break;
                }
                exit();
            default:
                exit();
        }
    }

    private static void start() {
        boolean keepDoingIt = true;

        log("Welcome to 'Marvel Super Heroes' Command Line Tool!\nPlease enter your <command> or press <Enter> to get the list of available commands. Write <exit> to terminate session.");
        while (keepDoingIt) {
            try {
                do {
                    p.log("\n>> ");
                } while ((keepDoingIt = doJob()));
            } catch (IOException ex) {
                log("Failed to connect to server: " + ex.getMessage());
            } catch (Throwable ex) {
                try {
                    log(toPrettyFormat(ex.getMessage()));
                } catch (Throwable e) {
                    log("Failed to pretty format response:\n" + ex.getMessage());
                }
            }
        }

        log("Goodbye and thank you for using our Command Line Tool!");
        System.exit(0);
    }

    private static void formatURIVariables() {
        //initialisation of the variables needed for the correct functioning of the application
        ADD = String.format(ADD, DEFAULT_SERVICE_URI, DEFAULT_USER);
        EDIT = String.format(EDIT, DEFAULT_SERVICE_URI, DEFAULT_USER);
        DELETE = String.format(DELETE, DEFAULT_SERVICE_URI, DEFAULT_USER);
        UPDATE = String.format(UPDATE, DEFAULT_SERVICE_URI, DEFAULT_USER);
        COMP = String.format(COMP, DEFAULT_SERVICE_URI, DEFAULT_USER);
        LISTC = String.format(LISTC, DEFAULT_SERVICE_URI, DEFAULT_USER);
        LISTN = String.format(LISTN, DEFAULT_SERVICE_URI, DEFAULT_USER);
    }

    /**
     * Since we use threads, using a synchronized object to print ensures that
     * we always print the entire response at a time before printing another.*
     */
    private static void log(String msg) {
        synchronized (p) {
            p.log(msg);
        }
    }

    /**
     * Adds the commands to a Map used to display custom messages when User
     * enters the associated command.*
     */
    private static void addCommands() {
        String helpAll = ">>> command (%s):\nDescription: Displays help for all existing commands.\nUsage: '%s'";
        helpAll = String.format(helpAll, helpAllCommandTag, helpAllCommandTag);
        String comp = ">>> command (%s):\nDescription: Displays a comparison between two creators.\nUsage: '%s {creatorId1} {creatorId2}'\nExample: '%s 5638 410'";
        comp = String.format(comp, compareCommandTag, compareCommandTag, compareCommandTag);
        String help = ">>> command (%s):\nDescription: Gets help about the command 'command_name'.\nUsage: '%s {command_name}'\nExample: '%s %s'";
        help = String.format(help, helpCommandTag, helpCommandTag, helpCommandTag, helpCommandTag);
        String add = ">>> command (%s):\nDescription: Adds a note to the creator.\nUsage: '%s {creatorId} {description}'\nExample: '%s 387 My custom note'";
        add = String.format(add, addCommandTag, addCommandTag, addCommandTag);
        String del = ">>> command (%s):\nDescription: Deletes the note.\nUsage: '%s {noteHash}'\nExample: '%s rAndomStringThatSeemsLikeMD5digest'";
        del = String.format(del, deleteCommandTag, deleteCommandTag, deleteCommandTag);
        String edit = ">>> command (%s):\nDescription: Updates the note description.\nUsage: '%s {noteHash} {description}'\nExample: '%s rAndomStringThatSeemsLikeMD5digest My updated note!'";
        edit = String.format(edit, editCommandTag, editCommandTag, editCommandTag);
        String listc = ">>> command (%s):\nDescription: Shows the list of creators.\nUsage: '%s {{optional:filters} {optional:sorters} {...}}'\n"
                + "Example1: '%s' : shows a list of all creators with default filters/sorters defined by the Marvel API\n"
                + "Example2: '%s sortByNotesNr=desc orderBy=firstName' : shows a list of all creators that are ordered by 'firstName' and sorted by the number of notes they have. "
                + "\n(Note: check README file for more information about filters/sorters.";
        listc = String.format(listc, listCreatorsCommandTag, listCreatorsCommandTag, listCreatorsCommandTag, listCreatorsCommandTag);

        parameters.put(helpAllCommandTag, helpAll);
        parameters.put(helpCommandTag, help);
        parameters.put(addCommandTag, add);
        parameters.put(editCommandTag, edit);
        parameters.put(deleteCommandTag, del);
        parameters.put(listCreatorsCommandTag, listc);
        parameters.put(compareCommandTag, comp);

    }

    /**
     * Gets the command if it exists, or shows the list of available commands*
     */
    private static String getCommand(String cmd, StringTokenizer st) throws IOException, HttpCallException {
        if (parameters.get(cmd) != null) {
            return commandOption(cmd, st);
        } else {
            log(String.format(invalidCommand, cmd));
            return availableCommands();
        }
    }

    /**
     * Depending on the command entered, the respective action is triggered.*
     */
    private static String commandOption(String cmd, StringTokenizer st) throws IOException, HttpCallException {
        switch (cmd) {
            case helpCommandTag:
                return help(cmd, st, 1);
            case helpAllCommandTag:
                return printAllCommands(st, 0);
            case addCommandTag:
                return add(st, 2);
            case editCommandTag:
                return edit(st, 2);
            case deleteCommandTag:
                return del(st, 1);
            case compareCommandTag:
                return comp(st, 2);
            case listCreatorsCommandTag:
                return listc(st, PRETTY);
            case listNotesCommandTag:
                return listn(st, PRETTY);
            default:
                log(String.format(invalidCommand, cmd));
                break;
        }
        return "";
    }

    /**
     * Convert a JSON string to pretty print version
     *
     * @param jsonString
     * @return
     */
    private static String toPrettyFormat(String jsonString) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonString).getAsJsonObject();
        String prettyJson = gson.toJson(json);
        return prettyJson;
    }

    private static String printCommandsDescription() {
        StringBuilder sb = new StringBuilder();

        for (Entry e : parameters.entrySet()) {
            sb.append("\n-------------------\n");
            sb.append(e.getValue());
        }
        sb.append("\n-------------------");
        return sb.toString();
    }

    private static String printAllCommands(StringTokenizer st, int argNumber) {
        if (st.countTokens() == argNumber) {
            return printCommandsDescription();
        }
        log(String.format(invalidArgNumber, argNumber, st.countTokens()));
        return "";
    }

    private static String help(String cmd, StringTokenizer st, int argNumber) throws IOException, HttpCallException {
        if (st.countTokens() == argNumber - 1) {
            return parameters.get(cmd);
        } else if (st.countTokens() > argNumber) {
            log(String.format(invalidArgNumber, argNumber, st.countTokens()));
            return "";
        }
        return printHelp(st.nextToken());
    }

    private static String printHelp(String cmd) throws IOException, HttpCallException {
        if (parameters.get(cmd) != null) {
            return parameters.get(cmd);
        } else {
            log(String.format(invalidCommand, cmd));
            return availableCommands();
        }
    }

    private static String listc(StringTokenizer st, boolean pretty) throws IOException, HttpCallException {
        log(doRequest(extractParametersToGET(st).insert(0, LISTC).toString(), "GET", contentType, connectionTimeout, pretty));
        return "";
    }

    private static String listn(StringTokenizer st, boolean pretty) throws IOException, HttpCallException {
        log(doRequest(extractParametersToGET(st).insert(0, LISTN).toString(), "GET", contentType, connectionTimeout, pretty));
        return "";
    }

    private static String add(StringTokenizer st, int argNumber) throws IOException, HttpCallException {
        if (st.countTokens() > argNumber - 1) {
            String json = extractParametersAsJsonString(addKeys, st);
            Client.doRequest(ADD, json, "POST", contentType, connectionTimeout, PRETTY);
        } else {
            log(String.format(invalidArgNumber, argNumber, st.countTokens()));
        }
        return "";
    }

    private static String del(StringTokenizer st, int argNumber) throws IOException, HttpCallException {
        if (st.countTokens() > argNumber - 1) {
            String json = extractParametersAsJsonString(delKeys, st);
            Client.doRequest(DELETE, json, "DELETE", contentType, connectionTimeout, PRETTY);
        } else {
            log(String.format(invalidArgNumber, argNumber, st.countTokens()));
        }
        return "";
    }

    private static String edit(StringTokenizer st, int argNumber) throws IOException, HttpCallException {
        if (st.countTokens() > argNumber - 1) {
            String json = extractParametersAsJsonString(editKeys, st);
            Client.doRequest(UPDATE, json, "PUT", contentType, connectionTimeout, PRETTY);
        } else {
            log(String.format(invalidArgNumber, argNumber, st.countTokens()));
        }
        return "";
    }

    /**
     * * Because the call to this service can take too long to complete (I have
     * experienced 1 minute of waiting until getting a Response, even in Marvel
     * API directly the call takes a very long time), I used a CompletableFuture
     * to do the call without the need for the user to block and wait for the
     * Response, so one can keep issuing requests to the server.
     *
     * * The boolean "hasStarted" is used just to get the main thread to print
     * the ' >> ' only after the FutureThread prints the 'Request URL >>> (...)'
     * . In one word: Aesthetics! :)*
     */
    private static String comp(StringTokenizer st, int argNumber) throws IOException, HttpCallException {
        hasStarted = false;
        if (st.countTokens() == argNumber) {
            CompletableFuture futureCount = CompletableFuture.supplyAsync(
                    () -> {
                        try {
                            String requestURL = COMP + "/" + st.nextToken() + "/" + st.nextToken();
                            log("Request URL >>> " + requestURL);
                            hasStarted = true;
                            log(doRequestAsync(requestURL, "GET", contentType, connectionTimeout, PRETTY));
                        } catch (IOException | HttpCallException ex) {
                            hasStarted = true;
                            return "Failed to obtain response from API call...";
                        }
                        return "";
                    });

            while (!hasStarted && !futureCount.isCancelled()) {
            }

        } else {
            log(String.format(invalidArgNumber, argNumber, st.countTokens()));
        }
        return "";
    }

    private static String getHttpResponse(String urlStr, String method, String contentType, int connectionTimout, boolean pretty) throws IOException, HttpCallException {
        if (pretty) {
            return toPrettyFormat(httpCaller.getHttpResponse(urlStr, method, contentType, connectionTimout, debugRequests));
        } else {
            return httpCaller.getHttpResponse(urlStr, method, contentType, connectionTimout, debugRequests);
        }
    }

    private static String getHttpResponse(String urlStr, String json, String method, String contentType, int connectionTimout, boolean pretty) throws IOException, HttpCallException {
        if (pretty) {
            return toPrettyFormat(httpCaller.getHttpResponse(urlStr, json, method, contentType, connectionTimout, debugRequests));
        } else {
            return httpCaller.getHttpResponse(urlStr, json, method, contentType, connectionTimout, debugRequests);
        }
    }

    /**
     * Builds a json string from the args, to use as a request in the methods
     * that use a body.*
     */
    private static String extractParametersAsJsonString(String[] keys, StringTokenizer st) {
        StringBuilder queryParameters = new StringBuilder("{\"");
        queryParameters.append(keys[0]);

        if (keys.length > 1) {
            queryParameters.append("\":");
            queryParameters.append(escapeQuotes(st.nextToken()));
            queryParameters.append(",\"");
            queryParameters.append(keys[1]);
            queryParameters.append("\":\"");
            if (st.hasMoreTokens()) {
                queryParameters.append(escapeQuotes(st.nextToken()));
                while (st.hasMoreTokens()) {
                    queryParameters.append(" ");
                    queryParameters.append(escapeQuotes(st.nextToken()));
                }
            }
        } else {
            queryParameters.append("\":\"");
            queryParameters.append(escapeQuotes(st.nextToken()));
        }

        queryParameters.append("\"}");

        return queryParameters.toString();
    }

    /**
     * In case our note description has quotes, we need to escape them.*
     */
    private static String escapeQuotes(String toEscape) {
        return toEscape.replace("\"", "\\\"");
    }

    /**
     * Extracts the parameters received and build a queryParam string *
     */
    private static StringBuilder extractParametersToGET(StringTokenizer st) {
        StringBuilder queryParameters = new StringBuilder();
        if (st.hasMoreTokens()) {
            queryParameters.append("?");
            queryParameters.append(st.nextToken());
            while (st.hasMoreTokens()) {
                queryParameters.append("&");
                queryParameters.append(st.nextToken());
            }
        }
        return queryParameters;
    }

    private static String doRequest(String requestURL, String method, String contentType, int connectionTimout, boolean pretty) throws IOException, HttpCallException {
        log("Request URL >>> " + requestURL);
        return getHttpResponse(requestURL, method, contentType, connectionTimout, pretty);
    }

    private static String doRequestAsync(String requestURL, String method, String contentType, int connectionTimout, boolean pretty) throws IOException, HttpCallException {
        return getHttpResponse(requestURL, method, contentType, connectionTimout, pretty);
    }

    private static void doRequest(String requestURL, String json, String method, String contentType, int connectionTimout, boolean pretty) throws IOException, HttpCallException {
        log("Request URL >>> " + requestURL);
        log(getHttpResponse(requestURL, json, method, contentType, connectionTimout, pretty));
    }

    private static String availableCommands() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nAvailable Commands:\n");

        for (Map.Entry e : parameters.entrySet()) {
            sb.append(e.getKey());
            sb.append("\n");
        }

        sb.append("\nUse help 'command_name' to get help about the command!");

        return sb.toString();
    }

    private static void printUsage() {
        log("--------- Usage: ----------");
        log("Connecto to local server args list: ['local' 'ip' 'port' 'appName' 'appVersion' 'username']"
                + "\nConnecto to remote server args list: ['remote' 'subdomain/appName' 'domain' 'appVersion' 'username']");
        log("E.g. if using Maven:"
                + "\n>> $ mvn exec:java -Dexec.args=\"local localhost 8080 MarvelSuperHeroesAPI v1 Pedro\"");
        log(">> $ mvn exec:java -Dexec.args=\"remote marvel-super-heroes-api herokuapp.com v1 Pedro\"");
    }

    /**
     * While the user does not exit the App, keep asking for jobs.*
     */
    private static boolean doJob() throws IOException, HttpCallException {
        bi = new BufferedReader(new InputStreamReader(System.in));
        String line, cmd;

        if ((line = bi.readLine()) != null) {
            if (!line.isEmpty()) {
                st = new StringTokenizer(line);
                cmd = st.nextToken();
                if (cmd.equals(exitTag)) {
                    return false;
                } else {
                    log(getCommand(cmd, st));
                }
            } else {
                log(availableCommands());
            }
        } else {
            log("Failed to read command! Exiting.");
            return false;
        }
        return true;
    }

    private static void printRemoteInfo(String subdomain, String domain, String version, String username) {
        log("--------------");
        log("Accessing remote version of the application.\n--------------");
        log("Subdomain: " + subdomain);
        log("Domain: " + domain);
        log("Version: " + version);
        log("Username: " + username);
        DEFAULT_SERVICE_VERSION = version;
        DEFAULT_USER = username;
        DEFAULT_SERVICE_URI = String.format(SERVER_REMOTE_ADDRESS_FORMAT, subdomain, domain, DEFAULT_SERVICE_VERSION);
        log("Service uri: " + DEFAULT_SERVICE_URI);
        log("--------------");
    }

    private static void printLocalInfo(String hostanme, String port, String appName, String version, String username) {
        log("--------------");
        log("Accessing local version of the application.\n");
        log("hostname: " + hostanme);
        log("port: " + port);
        log("appName: " + appName);
        log("Version: " + version);
        log("Username: " + username);
        DEFAULT_SERVICE_VERSION = version;
        DEFAULT_USER = username;
        DEFAULT_SERVICE_URI = String.format(SERVER_LOCAL_ADDRESS_FORMAT, hostanme, port, appName, DEFAULT_SERVICE_VERSION);
        log("Service uri: " + DEFAULT_SERVICE_URI);
        log("--------------");
    }

    private static void exit() {
        printUsage();
        log("----------Exiting----------");
        System.exit(0);
    }

}

class Printer {

    public Printer() {
    }

    public synchronized void log(String msg) {
        System.out.println(msg);
        System.out.flush();
    }
}
