package com.pgeadas.marvelcmd;

/**
 *
 * @author geadas
 */
public class HttpCallException extends Throwable {

    private final int statusCode;

    /**
     *
     * @param message
     * @param statusCode
     */
    public HttpCallException(String message, int statusCode) {
        super(message);
        this.statusCode = statusCode;
    }

    /**
     *
     * @return
     */
    public int getStatusCode() {
        return statusCode;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

}
