package com.pgeadas.marvelcmd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import org.json.JSONObject;

/**
 *
 * @author geadas
 */
public class HttpCaller {

    private final String method;
    private final String contentType;
    private final int connectTimeout;
    private final boolean doDebug = true;
    private static Printer printer;

    /**
     *
     * @param method
     * @param contentType
     * @param connectTimeout
     * @param p
     */
    public HttpCaller(String method, String contentType, int connectTimeout, Printer p) {
        this.method = method;
        this.contentType = contentType;
        this.connectTimeout = connectTimeout;
        printer = p;
    }

    /**
     *
     * @param p
     */
    public HttpCaller(Printer p) {
        this.method = "GET";
        this.contentType = "application/json";
        this.connectTimeout = 5000;
        printer = p;
    }

    /**
     * Makes an HTTP Request with the parameters set when this Class was
     * instantiated.
     *
     *
     * @param urlStr
     * @return
     * @throws java.io.IOException
     * @throws com.pgeadas.marvelcmd.HttpCallException
     */
    public String getDefaultHttpResponse(String urlStr) throws IOException, HttpCallException {
        return getHttpResponse(urlStr, method, contentType, connectTimeout, doDebug);
    }

    /**
     * Method used to call the Marvel API and retrieve the response from the
     * service
     *
     *
     * @param urlStr
     * @param method
     * @param contentType
     * @param connectTimeout
     * @param debug
     * @return
     * @throws java.io.IOException TODO: put this hard coded properties in the
     * properties file
     * @throws com.pgeadas.marvelcmd.HttpCallException
     */
    public String getHttpResponse(String urlStr, String method, String contentType, int connectTimeout, boolean debug) throws IOException, HttpCallException {
        boolean error = false;
        URL url = new URL(urlStr);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is;
        connection.setRequestMethod(method);
        connection.setRequestProperty("Accept", contentType);
        connection.setConnectTimeout(connectTimeout);
        //conn.setReadTimeout(20000);
        connection.setDoOutput(true);

        if (debug) {
            printer.log("\nReceived <<< " + connection.getHeaderFields().toString());
        }

        try {
            //its ok
            is = connection.getInputStream();
        } catch (IOException ex) {
            //error
            is = connection.getErrorStream();
            error = true;
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (is)));

        StringBuilder outputBuilder = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            outputBuilder.append(output);
        }
        connection.disconnect();

        String result = outputBuilder.toString();

        //above, we get the message before throwing the exception
        if (error) {
            throw new HttpCallException(result, connection.getResponseCode());
        }

        return result;
    }

    /**
     * @param urlStr
     *
     * @param json
     * @param connectTimeout
     * @param method
     * @param contentType
     * @param debug
     * @return
     * @throws java.io.IOException
     * @throws com.pgeadas.marvelcmd.HttpCallException
     */
    public String getHttpResponse(String urlStr, String json, String method, String contentType, int connectTimeout, boolean debug) throws IOException, HttpCallException {
        boolean error = false;
        URL url = new URL(urlStr);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream is;
        connection.setRequestMethod(method);
        connection.setRequestProperty("Content-Type", contentType);
        connection.setConnectTimeout(connectTimeout);
        connection.setDoOutput(true);

        if (debug) {
            printer.log("\nSending >>> " + json);
        }

        try (OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream())) {
            out.write(json);
        }
        if (debug) {
             printer.log("\nReceived <<< " + connection.getHeaderFields().toString());
        }

        try {
            //its ok
            is = connection.getInputStream();
        } catch (IOException ex) {
            //error
            is = connection.getErrorStream();
            error = true;
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
                (is)));

        StringBuilder outputBuilder = new StringBuilder();
        String output;
        while ((output = br.readLine()) != null) {
            outputBuilder.append(output);
        }
        connection.disconnect();

        String result = outputBuilder.toString();

        //above, we get the message before throwing the exception
        if (error) {
            throw new HttpCallException(result, connection.getResponseCode());
        }

        return result;
    }
}
