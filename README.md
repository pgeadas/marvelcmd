# **Project Component 2: Command Line Tool (MarvelCMD)**

The goal of the command line tool is to provide an interface to the API. It is very easy to use and it is self explanatory how to do so, since there is an 'help' command that describes each supported operation and its usage.

##Cloning the project

```
$ git clone https://pmrg@bitbucket.org/pmrg/marvelcmd.git
```

## Building the project



To run the tool, go to its root folder and run the command below. This will build our client .jar file.

```
$ mvn clean package
```

 Next, simply run:

```
$ mvn exec:java
```

This will start the tool and you will this in the console:

```
--------- Usage: ----------
Connecto to local server args list: ['local' 'ip' 'port' 'appName' 'appVersion' 'username']
Connecto to remote server args list: ['remote' 'subdomain/appName' 'domain' 'appVersion' 'username']
E.g. if using Maven:
>> $ mvn exec:java -Dexec.args="local localhost 8080 MarvelSuperHeroesAPI v1 Pedro"
>> $ mvn exec:java -Dexec.args="remote marvel-super-heroes-api herokuapp.com v1 Pedro"
----------Exiting----------
```
This means that the number of arguments provided was not correct. The correct usage is then displayed.
From here all is self explanatory and the tool will do requests to the predefined service URL's. 

Providing the correct number of arguments you will then see:

```
--------------
Accessing remote version of the application.
--------------
Subdomain: marvel-super-heroes-api
Domain: herokuapp.com
Version: v1
Username: Pedro
Service uri: http://marvel-super-heroes-api.herokuapp.com/v1
--------------
Welcome to 'Marvel Super Heroes' Command Line Tool!
Please enter your <command> or press <Enter> to get the list of available commands. Write <exit> to terminate session.

>>
```

## Using the tool to call the API

As an example, pressing ```<Enter>``` will result in:

```
Available Commands:
help-all
help
add
edit
del
listc
comp

Use help 'command_name' to get help about the command!

>>
```

To get help about a command:

```
>> help add
>>> command (add):
Description: Adds a note to the creator.
Usage: 'add {creatorId} {description}'
Example: 'add 387 My custom note'

>>
```